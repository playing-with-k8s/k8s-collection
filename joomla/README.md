# Joomla

## Requirements
- minikube installed
- ingress addon enabled
- repo cloned on you local

## Docker images
- [joomla](https://hub.docker.com/_/joomla)
- [mysql](https://hub.docker.com/_/mysql)


## Deploy app
Clean your kubernetes environment:
Note: not needed the first time.
```sh
kubectl delete namespace/joomla
kubectl delete pv mysql-pv-volume
```


## Declarative mode
```sh
cd joomla
kubectl create -f joomla-namespace.yml
kubectl create -f joomla-configmap.yml
kubectl create -f joomla-secrets.yml
kubectl create -f mysql-statefulset.yml
kubectl create -f mysql-headless-service.yml
kubectl create -f joomla-deployment.yml 
kubectl create -f joomla-service.yml 
kubectl create -f joomla-ingress.yml
```

or

```sh
cd joomla
kubectl create -f .
```

## Imperative mode
TODO: Capture and list all commands required to get the app deployed. So we are going to see the differences between declaravice vs imperative.
